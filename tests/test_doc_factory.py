#!/usr/bin/env python
"""Tests for `doc_factory` package."""
# pylint: disable=redefined-outer-name

import pytest
from click.testing import CliRunner

from doc_factory import cli
from doc_factory.template import Template


@pytest.fixture
def response():
    """Sample pytest fixture.

    See more at: http://doc.pytest.org/en/latest/fixture.html
    """


def test_execute_template():
    test_cases = [
        {
            'subtest': 'Simple variable 1',
            'template': '{{ variable }}',
            'data': {'variable': 'hello'},
            'expected': 'hello',
            'source': 'string'
        },
        {
            'subtest': 'Simple variable 2',
            'template': '{{ variable }} world!',
            'data': {'variable': 'hello'},
            'expected': 'hello world!',
            'source': 'string'
        },
        {
            'subtest': 'Simple file 1',
            'template': 'hello_world.txt',
            'data': {'variable': 'hello'},
            'expected': 'hello world!',
            'source': 'file'
        },
        {
            'subtest': 'Nested variable 1',
            'template': "{{ variable.nom }} world!",
            'data': {'variable': {'nom': 'hello'}},
            'expected': 'hello world!',
            'source': 'string'
        }
    ]

    for test_case in test_cases:
        assert Template(
            test_case['template'],
            source=test_case['source'], directory='tests'
        ).execute(test_case['data']) == test_case['expected'], test_case['subtest']



def test_command_line_interface():
    """Test the CLI."""
    runner = CliRunner()
    result = runner.invoke(cli.main)
    assert result.exit_code == 0
    assert 'doc_factory.cli.main' in result.output
    help_result = runner.invoke(cli.main, ['--help'])
    assert help_result.exit_code == 0
    assert '--help  Show this message and exit.' in help_result.output
