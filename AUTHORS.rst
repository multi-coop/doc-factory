=======
Credits
=======

Development Lead
----------------

* Quentin Loridant  <quentin.loridant@multi.coop>

Contributors
------------

None yet. Why not be the first?
