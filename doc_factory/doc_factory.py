"""Main module."""

from jinja2 import Environment, PackageLoader, select_autoescape


def execute_template(template, data):
    """Combine data and string template into a string

    Data validation is out of the scope of this function.

    Example:
        execute_template('{{ variable }} world!', {'variable': 'hello'})

    Attributes:
        template (string): follows Jinja2 template syntax
        data (dict): data to be used in template
    """
    env = Environment(
        loader=PackageLoader("doc_factory"),
        autoescape=select_autoescape()
    )
    jinja_template = env.from_string(template)
    return jinja_template.render(**data)
