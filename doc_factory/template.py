from jinja2 import Environment, PackageLoader, select_autoescape

import json


class Template:
    def __init__(self, template, source, directory="doc_factory"):
        self.template = template
        self.source = source
        self.directory = directory

    def execute(self, data):
        """Combine data and template into a string

        Data validation is out of the scope of this function.

        Example:
            execute_template('{{ variable }} world!', {'variable': 'hello'})

        Attributes:
            template (string): follows Jinja2 template syntax
            source (dict): data to be used in template
        """
        env = Environment(
            loader=PackageLoader(self.directory),
            autoescape=select_autoescape()
        )
        if self.source == "string":
            jinja_template = env.from_string(self.template)
        elif self.source == "file":
            jinja_template = env.get_template(self.template)

        return jinja_template.render(**data)


if __name__ == '__main__':
    with open('./doc_factory/templates/data_factsheet.json') as f:
        data = json.load(f)
    render = Template('example_factsheet.tex', source='file').execute(data)
    with open('data_factsheet.html', 'w') as f:
        f.write(render)
