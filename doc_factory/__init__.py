"""Top-level package for doc-factory."""

__author__ = """Quentin Loridant """
__email__ = 'quentin.loridant@multi.coop'
__version__ = '0.1.0'
