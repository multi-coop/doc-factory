===========
doc-factory
===========


.. image:: https://img.shields.io/pypi/v/doc_factory.svg
        :target: https://pypi.python.org/pypi/doc_factory

.. image:: https://img.shields.io/travis/qloridant/doc_factory.svg
        :target: https://travis-ci.com/qloridant/doc_factory

.. image:: https://readthedocs.org/projects/doc-factory/badge/?version=latest
        :target: https://doc-factory.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Combine data with a template to produce an output document.


* Free software: MIT
* Documentation: https://doc-factory.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `briggySmalls/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`briggySmalls/cookiecutter-pypackage`: https://github.com/briggySmalls/cookiecutter-pypackage
