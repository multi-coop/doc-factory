.. highlight:: shell

============
Installation
============


Stable release
--------------

To install doc-factory, run this command in your terminal:

.. code-block:: console

    $ pip install doc_factory

This is the preferred method to install doc-factory, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From source
-----------

The source for doc-factory can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://github.com/qloridant/doc_factory

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://github.com/qloridant/doc_factory/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ pip install .

.. _Github repo: https://github.com/qloridant/doc_factory
.. _tarball: https://github.com/qloridant/doc_factory/tarball/master
